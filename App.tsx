import SplashScreen from 'react-native-splash-screen'

import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import Navigations from './src';

const App = () => {
  useEffect(()=>{
    SplashScreen.hide();
  },[])
  return (
    <SafeAreaView
    style={StyleSheet.flatten({flex: 1, backgroundColor: Colors.lighter})}>
    <StatusBar barStyle={'dark-content'} backgroundColor={Colors.lighter} />
    
     <Navigations/>
    
   
  </SafeAreaView>
  );
};

export default App;
