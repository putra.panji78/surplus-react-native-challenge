import React, { useEffect, useState } from "react";
import { Image } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { Discover, Login } from "../screens";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainTabNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: "#009788",
      }}
    >
      <Tab.Screen
        name="DISCOVER"
        component={Discover}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={{
                  uri: focused
                    ? "https://i.postimg.cc/ThGHDM8p/Screenshot-2023-05-11-at-21-36-10.png"
                    : "https://i.postimg.cc/8sHXdKDw/Screenshot-2023-05-11-at-21-37-13.png",
                }}
                style={{
                  height: 20,
                  width: 20,
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="PESANAN"
        component={Discover}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={{
                  uri: focused
                    ? "https://i.postimg.cc/K3ZqSL7n/Screenshot-2023-05-11-at-21-37-27.png"
                    : "https://i.postimg.cc/k6N1pwSJ/Screenshot-2023-05-11-at-21-37-46.png",
                }}
                style={{
                  height: 20,
                  width: 20,
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="FORUM"
        component={Discover}
        options={{
          tabBarIcon: () => {
            return (
              <Image
                source={{
                  uri: "https://i.postimg.cc/Q9ZYmvHY/Screenshot-2023-05-11-at-21-37-54.png",
                }}
                style={{
                  height: 20,
                  width: 20,
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Profil"
        component={Discover}
        options={{
          tabBarIcon: () => {
            return (
              <Image
                source={{
                  uri: "https://i.postimg.cc/HrKhcrHg/Screenshot-2023-05-11-at-21-38-18.png",
                }}
                style={{
                  height: 20,
                  width: 20,
                }}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};
const Navigations = () => {
  const [token, setToken] = useState("");

  useEffect(() => {
    const getToken = async () => {
      try {
        const value = await AsyncStorage.getItem("surplus_token");
        if (value) {
          setToken(value);
        }
      } catch (err) {}
    };

    getToken();
  }, []);
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={token ? "Main" : "Login"}
        screenOptions={{
          headerShown: false,
        }}
      >
        {token ? (
          <Stack.Screen name="Main" component={MainTabNav} />
        ) : (
          <>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Main" component={MainTabNav} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default Navigations;
