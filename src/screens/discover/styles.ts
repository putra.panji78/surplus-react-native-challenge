import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchWrapper: {
    width: '92%',
    backgroundColor: "white",
    paddingHorizontal: '4%',
    marginTop: -20,
    alignSelf: "center",
    paddingVertical: 12,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  searchText:{
    fontSize: 12,
    color: "#DDDDDD",
  },
  carouselWrapper:{
    width: "92%",
    alignSelf: "center",
    backgroundColor: "red",
    height: 200,
    marginTop: 32
  },
  contentSaldo:{
    width: "92%",
    paddingVertical: 12,
    alignSelf: "center",
    paddingHorizontal: "4%",
    borderRadius: 8,
    marginTop: 24,
    backgroundColor: "#F4F4F4"
  },
  cashIcon:{
    width: 16,
    height: 16,
    resizeMode: "contain",
    marginRight: 12
  },
  lineVertical:{
    width: 1,
    height: 32,
    backgroundColor: "#CACACA",
  },
  amountText:{
    fontSize: 12,
    color: "#009488",
    marginBottom: 4
  },
  surplusPayText:{
    fontSize: 12,
    color: "#B4B4B4",
  },
  noVouchersText:{
    fontSize: 12,
    color: "#009488",
    textAlign: "center"
  }
});
export default Styles;
