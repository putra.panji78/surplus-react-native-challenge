import { StyleSheet} from "react-native";

const Styles = StyleSheet.create({
    headerWrapper:{
        width: "100%",
        paddingHorizontal: "4%",
        paddingVertical: 16,
        backgroundColor: "#009488",
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16
    },
    row:{
        flexDirection: "row",
        alignItems: "center",
    },
    lokasiKamuText:{
        fontSize: 10,
        color: "white",
        marginRight: 6
    },
    chevronDownIcon:{
        width: 3,
        height: 3,
        resizeMode: "contain",
        backgroundColor: "white"
    },
    currentLocation:{
        fontSize:14,
        color:"white",
        fontWeight: "bold",
        marginTop: 10
    },
    headerRightIcon:{
        width: 20,
        height: 20,
        resizeMode: "contain",
        backgroundColor: "white",
        borderRadius: 10
    },
    hiText:{
        fontSize: 20,
        color: "white",
        fontWeight: "bold",
        marginTop: 20,
        marginBottom: 30
    }
})
export default Styles