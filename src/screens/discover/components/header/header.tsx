import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import Styles from './styles'
const Header = () =>{
    return(
        <View style={Styles.headerWrapper}>
        <View
          style={StyleSheet.flatten({
            ...Styles.row,
            justifyContent: 'space-between',
          })}>
          <View>
            <View style={Styles.row}>
              <Text style={Styles.lokasiKamuText}>Lokasi Kamu</Text>
              <View style={Styles.chevronDownIcon} />
            </View>
            <Text style={Styles.currentLocation}>Jl Baitis Salma 1</Text>
          </View>
          <View style={Styles.row}>
            <View
              style={StyleSheet.flatten({
                ...Styles.headerRightIcon,
                marginRight: 14,
              })}
            />
            <View style={Styles.headerRightIcon} />
          </View>
        </View>
        <Text style={Styles.hiText}>
              Hi, Putra Panji Wicaksono
        </Text>
      </View>
    )
}
export default Header