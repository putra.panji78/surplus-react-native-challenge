import React from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
} from "react-native";
import Styles from "./styles";
const Category = ({data}: {data:{image: string}[]}) => {
  const { width } = Dimensions.get("screen");
  return (
    <View style={Styles.container}>
      <Text style={Styles.categoryText}>Kategori</Text>
      <View style={Styles.row}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {data.map((item: any, idx: number) => (
            <Image
              key={idx}
              source={{ uri: item.image }}
              style={StyleSheet.flatten({
                ...Styles.contentWrapper,
                marginLeft: idx === 0 ? width * 0.04 : 0,
              })}
            />
          ))}
        </ScrollView>
      </View>
    </View>
  );
};
export default Category;
