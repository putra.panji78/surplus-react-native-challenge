import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
    container:{
        width:"100%",
    },
    categoryText:{
        fontSize: 14,
        color: "black",
        fontWeight: "bold",
        marginTop: 24,
        marginBottom: 16,
        paddingHorizontal: "4%"
    },
    row:{
        flexDirection: "row",
        alignItems: "center"
    },
    contentWrapper:{
        width: 90,
        height: 90,
        borderRadius: 8,
        marginRight: 8,
        backgroundColor: "red"
    }

})
export default Styles