import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
    container:{
        width: "100%",
        marginTop: 20
    },
    title:{
        fontSize: 14,
        color: "black",
        fontWeight: "bold",
    },
    description:{
        fontSize: 12,
        color: "#6F6F6F",
        marginTop: 4,
        marginBottom: 14
    },
    row:{
        flexDirection: "row",
        alignItems: "center"
    },
    card:{
        width: 130,
        borderRadius: 8,
        overflow: "hidden",
        marginRight: 8,
        backgroundColor: "white",
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderWidth: 1,
        borderColor: "#F6F6F6"
    },
    productImage:{
        width: "100%",
        height: 100,
        backgroundColor: "red",
    },
    brand:{
        width: 20,
        height: 20,
        borderRadius: 8,
        marginTop: -15,
        backgroundColor: "yellow",
        marginLeft: 12
    },
    distanceWrapper:{
        backgroundColor: "white",
        padding: 3,
        borderRadius: 5,
        marginTop: -40,
        marginRight: 12
    },
    distanceText:{
        fontSize: 12,
        color: "black",
    },
    stockText:{
        fontSize: 12,
        color: "#009488",
        marginTop: 10
    },
    productTitle:{
        fontSize: 12,
        color: "black",
        marginTop: 10,
        fontWeight: "bold"
    },
    openHours:{
        fontSize: 10,
        marginTop: 10,
        color: "#909090",
    },
    discountWrapper:{
        backgroundColor: "#F4E5E5",
        borderRadius: 8,
        padding: 3,
        marginRight: 5
    },
    discountText:{
        fontSize: 10,
        color: "#A5575D",
    },
    strikePrice:{
        fontSize: 10,
        color: "#C6C6C6",
        textDecorationLine: "line-through",
        marginRight: 5
    },
    price:{
        fontSize: 12,
        color: "black",
        fontWeight: "bold"
    },
    starIcon:{
        width: 10,
        height: 10,
        resizeMode: "contain",
        marginRight: 8
    },
    totalRatings:{
        fontSize: 10,
        color: "#90908F",
        marginRight: 8
    },
    lineVertical:{
        width: 1,
        height: 10,
        backgroundColor: "#90908F",
        marginRight: 8
    },
    newMenu:{
        fontSize: 10,
        color: "#009488"
    },
    allWrapper:{
        backgroundColor: "#E7F5F2",
        padding: 8,
        borderRadius: 14,
        marginRight: "4%"
    },
    allText:{
        fontSize: 12,
        color: "#70C2AD",
        fontWeight: "bold"
    }
})
export default Styles