import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
} from "react-native";
import Styles from "./styles";
interface IProducts {
  title: string;
  description: string;
  data: {
    stock: number;
    image: string;
    name: string;
    weight: string;
    openHours: string;
    percentageDiscount: number;
    price: number;
    rating: number;
    isNewMenu: boolean;
  }[];
}
const Products = ({ title, description, data }: IProducts) => {
  const { width } = Dimensions.get("screen");
  return (
    <View style={Styles.container}>
      <View
        style={StyleSheet.flatten({
          ...Styles.row,
          justifyContent: "space-between",
        })}
      >
        <View style={StyleSheet.flatten({ paddingLeft: "4%" })}>
          <Text style={Styles.title}>{title}</Text>
          <Text style={Styles.description}>{description}</Text>
        </View>
        <View style={Styles.allWrapper}>
          <Text style={Styles.allText}>Semua</Text>
        </View>
      </View>

      <View style={Styles.row}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {data &&
            data.map((item: any, idx: number) => (
              <View
                key={idx}
                style={StyleSheet.flatten({
                  ...Styles.card,
                  marginLeft: idx === 0 ? width * 0.04 : 0,
                })}
              >
                <Image
                  source={{ uri: item.image }}
                  style={Styles.productImage}
                />
                <View
                  style={StyleSheet.flatten({
                    ...Styles.row,
                    justifyContent: "space-between",
                  })}
                >
                  <View style={Styles.brand} />
                  <View style={Styles.distanceWrapper}>
                    <Text style={Styles.distanceText}>1 KM</Text>
                  </View>
                </View>
                <Text style={Styles.stockText}>{item.stock}+ Tersedia</Text>
                <Text style={Styles.productTitle}>
                  {item.name} {item.weight}
                </Text>
                <Text style={Styles.openHours}>
                  Ambil hari ini, {item.openHours}
                </Text>
                <View
                  style={StyleSheet.flatten({ ...Styles.row, marginTop: 10 })}
                >
                  <View style={Styles.discountWrapper}>
                    <Text style={Styles.discountText}>
                      {item.percentageDiscount}%
                    </Text>
                  </View>
                  {Boolean(item.percentageDiscount) && (
                    <Text style={Styles.strikePrice}>{item.price}</Text>
                  )}
                  <Text style={Styles.price}>
                    {item.price - item.price * (item.percentageDiscount / 100)}
                  </Text>
                </View>
                <View
                  style={StyleSheet.flatten({
                    ...Styles.row,
                    marginTop: 10,
                    marginBottom: 10,
                  })}
                >
                  <View style={Styles.row}>
                    <Image
                      style={Styles.starIcon}
                      source={{
                        uri: "https://img.uxwing.com/wp-content/themes/uxwing/download/arts-graphic-shapes/star-icon.png",
                      }}
                    />
                    <Text style={Styles.totalRatings}>
                      {item.rating.toFixed(1)}
                    </Text>
                    <View style={Styles.lineVertical} />
                    {item.isNewMenu && (
                      <Text style={Styles.newMenu}>Menu Baru!</Text>
                    )}
                  </View>
                </View>
              </View>
            ))}
        </ScrollView>
      </View>
    </View>
  );
};
export default Products;
