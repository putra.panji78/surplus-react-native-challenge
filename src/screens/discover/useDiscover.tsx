
const useDiscover = () =>{
    const categoryData = [
        {
          image:
            "https://i.postimg.cc/Js3gRZJR/Screenshot-2023-05-11-at-20-33-07.png",
        },
        {
          image:
            "https://i.postimg.cc/mc85H48q/Screenshot-2023-05-11-at-20-33-25.png",
        },
        {
          image:
            "https://i.postimg.cc/JD0d3FFh/Screenshot-2023-05-11-at-20-33-32.png",
        },
        {
          image:
            "https://i.postimg.cc/PLt7JJzY/Screenshot-2023-05-11-at-20-33-40.png",
        },
        {
          image:
            "https://i.postimg.cc/tZcvB50N/Screenshot-2023-05-11-at-20-34-43.png",
        },
        {
          image:
            "https://i.postimg.cc/V0bGpCmQ/Screenshot-2023-05-11-at-20-34-50.png",
        },
        {
          image:
            "https://i.postimg.cc/dD7xLzdH/Screenshot-2023-05-11-at-20-34-57.png",
        },
      ];
    const productData = [
        {
            stock: 10,
            image: "https://i.postimg.cc/QKpJZLYS/6098ef8f96efa.jpg",
            name: "Roti Sisir",
            weight: "250 G",
            openHours: "08.00 - 22.00",
            percentageDiscount: 50,
            price: 30000, 
            rating: 5.0,
            isNewMenu:true
        },
        {
            stock: 10,
            image: "https://i.postimg.cc/BXBBh0Kn/d109d4ab-fada-42f0-9d69-a3637cbfd00f.jpg",
            name: "Jus Melon",
            weight: "250 G",
            openHours: "08.00 - 22.00",
            percentageDiscount: 50,
            price: 30000, 
            rating: 5.0,
            isNewMenu:true
        },
        {
            stock: 10,
            image: "https://i.postimg.cc/nCJ1YzMW/d665b906-5953-4f22-955b-ffe78b197925.webp",
            name: "Roti Bluder",
            weight: "250 G",
            openHours: "08.00 - 22.00",
            percentageDiscount: 50,
            price: 30000, 
            rating: 5.0,
            isNewMenu:true
        },
        {
            stock: 10,
            image: "https://i.postimg.cc/QFNkwyQk/Resep-Dadar-Gulung-Pandan-Isi-Kelapa.jpg",
            name: "Dadar Gulung",
            weight: "250 G",
            openHours: "08.00 - 22.00",
            percentageDiscount: 50,
            price: 30000, 
            rating: 5.0,
            isNewMenu:true
        }

    ]
    return{
        data:{categoryData, productData},

    }
}
export default useDiscover