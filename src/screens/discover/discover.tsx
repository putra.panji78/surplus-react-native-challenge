import React from 'react';
import {Dimensions, Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import Styles from './styles';
import {Header, Category, Products} from './components'
import {ImageSlider} from 'react-native-image-slider-banner'
import useDiscover from './useDiscover';
const Discover = () => {
    const {
        data:{
            categoryData,
            productData
        }
    } = useDiscover()
    const {width} = Dimensions.get("screen")
  return (
    <View style={Styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>

       
        <Header/>
        <View style={Styles.searchWrapper}>
            <Text>
                Mau selamatkan makanan apa hari ini?
            </Text>
        </View>
        <ImageSlider
          data={[
            {img: 'https://i.postimg.cc/DmQSYMsq/Screenshot-2023-05-11-at-15-06-05.png'},
            {img: 'https://i.postimg.cc/SYGj2nqZ/Screenshot-2023-05-11-at-15-06-21.png'},
            {img: 'https://i.postimg.cc/tnbY430Z/Screenshot-2023-05-11-at-15-06-44.png'}
        ]}
          autoPlay={true}
          closeIconColor="#fff"
          activeIndicatorStyle={{
            width: 10,
            height:10,
            borderRadius: 5,
            backgroundColor: "#009488"
          }}
          caroselImageStyle={{
            width: width,

            height: 250
          }}
          inActiveIndicatorStyle={{
            width: 10,
            height:10,
            borderRadius: 5,
            backgroundColor: "#C4C4C4"
          }}
          indicatorContainerStyle={{
            alignSelf: "flex-start",
          }}
          indicatorWrapperStyle={{
            alignSelf: "flex-start",
            bottom: -15,
            marginLeft: "6%"
          }}
        />
       
        <View style={Styles.contentSaldo}>
          <View style={StyleSheet.flatten({...Styles.row, justifyContent: "space-around"})}>
            <View style={Styles.row}>
                <Image style={Styles.cashIcon} source={{uri: "https://i.postimg.cc/Bjh67L6n/Screenshot-2023-05-11-at-20-29-32.png"}}/>
                <View>
                    <Text style={Styles.amountText}>
                        Rp0
                    </Text>
                    <Text style={Styles.surplusPayText}>
                        Surplus Pay
                    </Text>
                </View>
            </View>
            <View style={Styles.lineVertical}/>
            <View style={Styles.row}>
            <Image style={Styles.cashIcon} source={{uri: "https://i.postimg.cc/5H6th4Yq/Screenshot-2023-05-11-at-20-29-58.png"}}/>
                <Text style={Styles.noVouchersText}>
                    Tidak ada voucher{'\n'} tersedia
                </Text>
            </View>
          </View>
        </View>

        <Category
            data={categoryData}
        />
        <Products 
        title='Di sekitar kamu' 
        description='Yuk selamatkan makanan di sekitarmu!'
        data={productData}
        />
        <Products
             title='Paling disuka' 
             description='Nikmati menu terfavorite'
             data={productData}
        />
        <Products
             title='Gak sampai 20rb' 
             description='makan enak tanpa khawatir boncos!'
             data={productData}
        />
        </ScrollView>
    </View>
  );
};
export default Discover;
