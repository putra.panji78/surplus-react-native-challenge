import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";
import { useState } from "react";
import { Alert } from "react-native";
const useLogin = () => {
  const navigation = useNavigation()
  const [form, setForm] = useState({
    email: "",
    password: "",
  });
  const [isShownPassword, setShownPassword] = useState(false);

  const handleShownPassword = (value: boolean) => {
    setShownPassword(value);
  };
  const handleChangeEmail = (email: string) => {
    setForm((prevState) => ({
      ...prevState,
      email,
    }));
  };
  const handleChangePassword = (password: string) => {
    setForm((prevState) => ({
      ...prevState,
      password,
    }));
  };
  const handleLogin = async () => {
    const email = "putra.panji78@gmail.com";
    const password = "SurplusIndonesia#1";
    try {
      if (form.email.trim() !== email || form.password.trim() !== password) {
        Alert.alert("Login Gagal", "Email atau Password Salah!");
        return;
      }
      await AsyncStorage.setItem("surplus_token", email);
    navigation.replace("Main")
    
      
    } catch (err) {
        console.log("ERRR====", err)
    }
  };
  return {
    method: {
      handleLogin,
      handleChangeEmail,
      handleChangePassword,
      handleShownPassword,
    },
    data: {isShownPassword, form },
  };
};
export default useLogin;
