import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
      },
      loginImage: {
        width: '100%',
        height: 250,
      },
      viewHeader: {
        position: 'absolute',
        bottom: 50,
        left: 30,
      },
      loginText:{
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
      },
      loginDescription: {
        color: '#fff',
        fontSize: 12,
        fontWeight: '400',
      },
      viewBottom: {
        backgroundColor: '#fff',
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        marginTop: -35,
        // position: 'absolute',
        bottom: 0,
        height: 650,
        width: '100%',
      },
      emailText: {
        color: '#000',
        marginLeft: 30,
        fontWeight: '500',
        marginTop: 20,
      },
      passwordText: {
        color: '#000',
        marginLeft: 30,
        fontWeight: '500',
        marginTop: 10,
      },
      passwordIcon: {
        position: 'absolute',
        right: 50,
        top: 50,
      },
      btnRegister: {
        backgroundColor: '#eeeeee',
        height: 50,
        width: '85%',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 20,
      },
      btnForgotPassword: {
        width: 100,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 30,
        marginTop: 10,
      },
      forgotText: {
        fontSize: 10,
        textDecorationLine: 'underline',
        width: 100,
        marginLeft: 20,
      },
      viewLogin: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
      },
      accountText: {
        fontSize: 12,
        color: '#000',
        fontWeight: '300',
      },
      registerText: {
        fontSize: 12,
        color: '#009788',
        fontWeight: '500',
        marginLeft: 5,
        textDecorationLine: 'underline',
      },
      textInput: {
        borderWidth: 1,
        borderColor: '#eeeeee',
        height: 50,
        width: '85%',
        borderRadius: 10,
        marginTop: 5,
        paddingLeft: 20,
        paddingRight: 10,
        flexDirection: 'row',
        alignSelf: 'center',
      },
      errorInput: {
        borderColor: 'red',
      },
      errorText: {
        color: 'red',
        fontSize: 12,
        marginTop: 5,
        marginLeft: 30,
        marginRight: 30,
      },
      passwordContainer: {
        position: 'relative',
      },
      confirmationText: {
        color: '#000',
        marginLeft: 30,
        fontWeight: '500',
        marginTop: 10,
      },
      lineHorizontal:{
        width: "40%",
        height: 1,
        backgroundColor: "#eeeeee"
      },
      row:{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: "8%",
        marginTop: 30
      },
      orText:{
        fontSize: 12,
        color: "#eeeeee",
        fontWeight: "bold"
      },
      loginWithSosmed:{
        width: "40%",
        backgroundColor: '#eeeeee',
        borderRadius: 20,
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
      },
      sosmedText:{
        fontSize: 12,
        color: "#3E3E3E",
      },
      sosmedIcon:{
        width: 14,
        height: 14,
        resizeMode: "contain",
        marginRight: 10
      }
})
export default Styles