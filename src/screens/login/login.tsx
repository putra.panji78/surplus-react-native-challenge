import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from "react-native";
import Styles from "./styles";
import useLogin from "./useLogin";
const Login = () => {
  const {
    data: { form, isShownPassword },
    method: {
      handleChangeEmail,
      handleShownPassword,
      handleChangePassword,
      handleLogin,
    },
  } = useLogin();
  return (
    <SafeAreaView style={Styles.container}>
      <View>
        <Image
          source={{ uri: "https://i.postimg.cc/0QPKMPZT/header-bg-min.png" }}
          style={Styles.loginImage}
        />
        <View style={Styles.viewHeader}>
          <Text style={Styles.loginText}>Masuk</Text>
          <Text
            style={StyleSheet.flatten({
              ...Styles.loginDescription,
              marginTop: 14,
            })}
          >
            Pastikan kamu sudah pernah
          </Text>
          <Text style={Styles.loginDescription}>membuat akun Surplus</Text>
        </View>
      </View>
      <View style={Styles.viewBottom}>
        <View>
          <Text style={Styles.emailText}>E-mail</Text>
          <TextInput
            autoCorrect={false}
            autoCapitalize="none"
            style={[
              Styles.textInput,
              { borderColor: form.email ? "#419489" : "#eeeeee" },
            ]}
            placeholder="Alamat email kamu"
            placeholderTextColor="#b2b2b2"
            value={form.email}
            onChangeText={handleChangeEmail}
          />
        </View>

        <View style={Styles.passwordContainer}>
          <Text style={Styles.passwordText}>Kata sandi</Text>
          <TextInput
            autoCorrect={false}
            autoCapitalize="none"
            style={[
              Styles.textInput,
              { borderColor: form.password ? "#419489" : "#eeeeee" },
            ]}
            placeholder="Masukkan kata sandi"
            placeholderTextColor="#b2b2b2"
            value={form.password}
            onChangeText={handleChangePassword}
            secureTextEntry={!isShownPassword}
          />
          <TouchableOpacity
            onPress={() => handleShownPassword(!isShownPassword)}
            style={Styles.passwordIcon}
          >
            <Image
              source={{
                uri: isShownPassword
                  ? "https://i.postimg.cc/HJzScbRj/eye-show.png"
                  : "https://i.postimg.cc/kVq1gTjk/eye-hide.png",
              }}
              style={StyleSheet.flatten({
                height: 20,
                width: 20,
                tintColor: "#b2b2b2",
              })}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity onPress={() => {}} style={Styles.btnForgotPassword}>
          <Text style={Styles.forgotText}>Lupa kata sandi?</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            Styles.btnRegister,
            {
              backgroundColor:
                form.email.length > 0 && form.password.length > 0
                  ? "#419489"
                  : "#eeeeee",
            },
          ]}
          disabled={!form.email.trim() || !form.password.trim()}
          onPress={handleLogin}
        >
          <Text
            style={{
              color: "#fff",
              fontWeight: "bold",
              fontSize: 15,
            }}
          >
            Masuk
          </Text>
        </TouchableOpacity>

        <View style={Styles.row}>
          <View style={Styles.lineHorizontal} />
          <Text style={Styles.orText}>Atau</Text>
          <View style={Styles.lineHorizontal} />
        </View>

        <View style={Styles.row}>
          <TouchableOpacity style={Styles.loginWithSosmed}>
            <Image
              style={Styles.sosmedIcon}
              source={{
                uri: "https://i.postimg.cc/sMmPFg47/Screenshot-2023-05-11-at-19-35-15.png",
              }}
            />
            <Text style={Styles.sosmedText}>Facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity style={Styles.loginWithSosmed}>
            <Image
              style={Styles.sosmedIcon}
              source={{
                uri: "https://i.postimg.cc/TKfjHDgT/Screenshot-2023-05-11-at-19-35-24.png",
              }}
            />
            <Text style={Styles.sosmedText}>Google</Text>
          </TouchableOpacity>
        </View>

        <View style={Styles.viewLogin}>
          <Text style={Styles.accountText}>Belum punya akun ?</Text>
          <TouchableOpacity onPress={() => {}}>
            <Text style={Styles.registerText}>Yuk daftar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default Login;
